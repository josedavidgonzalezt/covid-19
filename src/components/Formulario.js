import React, {useState} from 'react';

const Formulario = ({guardarPais,guardarPaisElegido}) => {

    const [error,guardarError]=useState(false);
    const [pais,actualizarPais]= useState('');

    const paises =[
        {codigo:'ar', nombre:'Argentina'},
        {codigo:'au', nombre:'Australia'},
        {codigo:'br', nombre:'Brasil'},
        {codigo:'cl', nombre:'Chile'},
        {codigo:'co', nombre:'Colombia'},
        {codigo:'ec', nombre:'Ecuador'},
        {codigo:'es', nombre:'España'},
        {codigo:'il', nombre:'Israel'},
        {codigo:'mx', nombre:'México'},
        {codigo:'pa', nombre:'Panamá'},
        {codigo:'pe', nombre:'Perú'},
        {codigo:'pt', nombre:'Portugal'},
        {codigo:'ve', nombre:'Venezuela'}
    ];
    

    const buscarContagios=e=>{
        e.preventDefault();
        if (pais.trim()===''){
            guardarError(true);
            return; 
        }

        guardarError(false)
        guardarPais(pais);
        
    }

    

    return ( 
        <form
            onSubmit={buscarContagios}
        >
            {error ?<p className="error">Selecciona un país</p> :null}
            <label className="label">País</label>
            <select
                className="select"
                onChange={e=>actualizarPais(e.target.value)}
            
            >
                <option value="">-Select-</option>
                {paises.map(pais=>(
                    <option key={pais.codigo} value={pais.codigo}>{pais.nombre}</option>
                ))}
            </select>
            <input
                className="boton"
                value="Buscar"
                type="submit"
            />
        </form>
     );
}
 
export default Formulario;