import React,{Fragment} from 'react';

const Casos = ({coronavirus}) => {
 
const {Country,Confirmed,Deaths,Recovered,Date}=coronavirus;
if (!Country || !Confirmed || !Deaths|| !Recovered || !Date ){return null;}



    return ( 
        <Fragment>
            <div>
               <h3>{Country}</h3>
               <h5>(Al {Date.slice(0,10)})</h5>
               <span></span>
               <p>Confirmados: {Confirmed}</p>
               <p>Muertes: {Deaths}</p>
               <p>Recuperados: {Recovered}</p>
            </div>
        </Fragment>
     );
}
 
export default Casos;

/* Country
Confirmed 
Deaths
Recovered 
Date */