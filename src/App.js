import React, {Fragment,useState,useEffect} from 'react';
import Formulario from './components/Formulario';
import Header from './components/Header';
import Casos from './Casos';

function App() {
  
  const [pais, guardarPais]=useState('');
  const [coronavirus,guardarCoronavirus]=useState({});

  useEffect(() => {
    const consultarAPI=async()=>{
      const url=`https://api.covid19api.com/dayone/country/${pais}`;
      const respuesta= await fetch(url);
      const coronavirus = await respuesta.json();
      guardarCoronavirus(coronavirus[coronavirus.length-1]);
      
    }
    
    consultarAPI();
  }, [pais])

  
    const casos =(!coronavirus) 
      ? null 
      : <div className="casos">
            <Casos
              coronavirus={coronavirus}
            />
        </div> 

  return (
    <Fragment>
      <div className="header">
        <Header
          titulo="Covid19"
        />
      </div>
      <div className="contenedor">
       <div className="formulario">
          <Formulario
            guardarPais={guardarPais}
          />
       </div>
        {casos}
      </div>
    </Fragment>
  );
}

export default App;
